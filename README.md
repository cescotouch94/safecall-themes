# SafeCall-themes

This project contains all the themes and assets required by SafeCall to be fully customized.

The main source of truth is the `themes.json` file, which specifies the version, the list of themes and assets required by each theme.
